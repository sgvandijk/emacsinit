#+TITLE: Emacs Configuration
#+AUTHOR: Sander van Dijk

* Installing Emacs

#+BEGIN_SRC sh
brew install emacs --with-cocoa --with-imagemagick --with-gnutls
#+END_SRC

Other possible options:
- ~--devel~ :: installs latest development version
- ~--HEAD~ :: installs latest Git head

* Personal Information

#+BEGIN_SRC emacs-lisp
(setq
  user-full-name "Sander van Dijk"
  user-mail-address "sgvandijk@gmail.com"
)
#+END_SRC

* Customize settings

Set up the customize file to its own separate file, instead of saving
customize settings in [[file:init.el][init.el]].
#+BEGIN_SRC emacs-lisp
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(load custom-file)
#+END_SRC

Load local configuration, such as platform dependent stuff
#+BEGIN_SRC emacs-lisp
(setq local-file (expand-file-name "local.el" user-emacs-directory))
(load local-file)
#+END_SRC
* Appearance
** Theme

Built in dark Tango palette
#+BEGIN_SRC emacs-lisp
(load-theme 'tango-dark)
#+END_SRC

# #+BEGIN_SRC emacs-lisp
# (use-package tangotango-theme
#   :ensure t
#   :config
#   (load-theme 'tangotango t t)
# )
# #+END_SRC

# Very popular theme
# #+BEGIN_SRC emacs-lisp
# (use-package zenburn-theme
#   :ensure t
#   :init
#   (load-theme 'zenburn t t))
# #+END_SRC

# Good-looking[tm] theme with quite fully-supported font-faces for
# various modes.
# #+BEGIN_SRC emacs-lisp
# (use-package moe-theme
#   :ensure t
#   :init
#   (progn
#     ;; (setq moe-theme-resize-markdown-title '(1.5 1.4 1.3 1.2 1.0 1.0))
#     (setq moe-theme-resize-org-title '(1.5 1.4 1.3 1.2 1.1 1.0 1.0 1.0 1.0))
#     ;;(setq moe-theme-resize-rst-title '(1.5 1.4 1.3 1.2 1.1 1.0))
#   )
#   :config
#   ;; (moe-dark)
# )
# #+END_SRC

# Nice and dark, but org-mode blocks are very distracting
# #+BEGIN_SRC emacs-lisp
# (use-package material-theme
#   :ensure t)
# #+END_SRC

** Font

The Liberation Mono font is compact but readable. It can be downloaded from [[https://fedorahosted.org/liberation-fonts/][Fedora Hosted]].
#+BEGIN_SRC emacs-lisp
(add-to-list 'default-frame-alist
             '(font . "Liberation Mono-12"))
#+END_SRC

* Defaults

A collection of defaults for soem global settings
#+BEGIN_SRC emacs-lisp
;; Keep all backup and auto-save files in one directory
(setq backup-directory-alist '(("." . "~/.emacs.d/backups")))
(setq auto-save-file-name-transforms '((".*" "~/.emacs.d/auto-save-list/" t)))

;; Turn off the blinking cursor
(blink-cursor-mode -1)

(setq-default indent-tabs-mode nil)
(setq-default indicate-empty-lines t)
(setq-default c-basic-offset 2)

(show-paren-mode t)
(column-number-mode t)

;; Turn off system bell
;; (setq visible-bell t) ;; Standard solution
(setq visible-bell nil)
(setq ring-bell-function 'ignore) ;; Solution to prevent ghosting on OSX

;; Use Cua mode for modern key bindings
(cua-mode t)

;; Enable line numbers in prog and config modes
(add-hook 'prog-mode-hook 'linum-mode)
(add-hook 'conf-mode-hook 'linum--mode)
#+END_SRC

* Magit

#+BEGIN_SRC emacs-lisp
(use-package magit
  :ensure t
  :bind ("C-x g" . magit-status)
  :init (setq magit-display-buffer-function
         #'magit-display-buffer-fullframe-status-v1))
#+END_SRC

* Ido

#+BEGIN_SRC emacs-lisp
(use-package ido
  :ensure t
  :init
  (setq ido-enable-flex-matching t)
  (setq ido-everywhere t)
  :config
  (ido-mode t))
#+END_SRC

* Multiple cursors

#+BEGIN_SRC emacs-lisp
(use-package multiple-cursors
  :ensure t
  :bind (
    ("C-S-c C-S-c" . mc/edit-lines)
    ("C->" . mc/mark-next-like-this)
    ("C-<" . mc/mark-previous-like-this)))
#+END_SRC

* Flycheck

#+BEGIN_SRC emacs-lisp
(use-package flycheck
  :ensure t
  :init
  (global-flycheck-mode))
#+END_SRC

* Org

#+BEGIN_SRC emacs-lisp
;; fontify code in code blocks
(setq org-src-fontify-natively t)

;; set some faces
(custom-set-faces
'(org-level-1 ((t (:inherit outline-§ :weight semi-bold :height 1.2))))
'(org-level-2 ((t (:inherit outline-2 :weight semi-bold :height 1.1))))
'(org-level-3 ((t (:inherit outline-3 :weight bold))))
'(org-level-5 ((t (:inherit outline-5))))
'(org-meta-line ((t (:inherit font-lock-comment-face :height 0.8))))
)
#+END_SRC

* Modes

** YAML

#+BEGIN_SRC emacs-lisp
(use-package yaml-mode
  :ensure t)
#+END_SRC

** Markdown

#+BEGIN_SRC emacs-lisp
(use-package markdown-mode
  :ensure t)
#+END_SRC

** OpenSCAD

#+BEGIN_SRC emacs-lisp
(use-package scad-mode
  :ensure t
  :mode "\\.scad\\'"
  :config
  (linum-mode 1))
#+END_SRC
